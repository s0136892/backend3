﻿<head>
	<title>Форма</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<link rel="stylesheet" href = "style.css">
	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</head>
<body>
	<div class="row">
	<div class="col-4"></div>
    <form action="" method="POST" class="col-4 brd">
    <div class="row align-items-center text-center">
			<h1 class="col header__headline">Форма</h1>
	</div>
    	<div class="form-group">
        	<p><b>Как вас зовут?</b></p>
      		<input name="fio" />
      	</div>
      	<div class="form-group">
      		<p><b> Оставьте ваш e-mail, мы свяжемся с вами:</b></p>
      		<input name="mail" /><br>
      	</div>
      	<div class="form-group">
      		<p><b>Ваш год рождения:</b></p>
      		<select name="year">
        		<?php for($i = 1960; $i < 2020; $i++) { ?>
        			<option value="<?php print $i; ?>"><?= $i; ?></option>
        		<?php } ?>
      		</select><br>
      	</div>
      	<div class="form-group">
          	<p><b> Ваш пол?</b></p>
         	<input name="gender" type="radio" value="men"> м
      		<input name="gender" type="radio" value="women" checked>ж 
      	</div> 
      	<div class="form-group">
      		<p><b> Количество конечностей:</b></p>
      		<input name="conech" type="radio" value="0" checked>0
    		<input name="conech" type="radio" value="1">1
    		<input name="conech" type="radio" value="2">2
    		<input name="conech" type="radio" value="3">3
    	</div>
    	<div class="form-group">
      		<p><b> Оставте ваш отзыв:</b></p>
      		<p><textarea name="comment"></textarea></p>
      	</div>
      	<div class="form-group">
      		<p><b> Выберите способность:</b></p>
      		<select name="abilities[]" multiple>
      			<?php 
                foreach ($ability_labels as $key => $value) {
                ?>
        		<option value="<?= $key; ?>"><?= $value; ?></option>
      			<?php } ?>
      		</select>
      	</div>
      	<div class="form-group">
      		<input type="checkbox" checked="checked" name="ask">с контрактом ознакомлен
      	</div>
      	<input type="submit" value="ok" />
    </form>
    	<div class="col-4"></div>
    	</div>
</body>